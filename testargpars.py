import argparse

print("Checking arguments ...")
parser = argparse.ArgumentParser()
parser.add_argument("type_v", help="type : p for picam driver, o for opencv, n for no display")
#parser.add_argument("type_track", help="type : f for fixed, t for track, s for single")
parser.add_argument("type_p", help="type : c or b")
parser.add_argument("horiz", help="c:azimut b:body")
parser.add_argument("vert",  help="c:altitude b:dummy ")
try:
    args = parser.parse_args()
    p_type_v = args.type_v
    p_type = args.type_p
    p_azimut = args.horiz
    p_altitude = args.vert
except :
    print("default argument")
    p_type_v = "n"
    p_type = "c"
    p_azimut = "0"
    p_altitude = "0"

print("Arg1 : ")
print(p_type_v)
print("Arg2 : ")
print(p_type)
print("Arg3 : ")
print(p_azimut)
print("Arg4 : ")
print(p_altitude)