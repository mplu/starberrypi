#!/usr/bin/python

import smbus
from gpiozero import LED
from time import sleep
import logging
import sys
import time
import atexit
import math
import astropy.units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, get_body
from threading import Thread
from Adafruit_BNO055 import BNO055
import curses
import atexit
import numpy as np
import cv2
import numpy as np
import argparse

def bearing255():
        bear = bus.read_byte_data(address, 1)
        return bear

def bearing3599():
        bear1 = bus.read_byte_data(address, 2)
        bear2 = bus.read_byte_data(address, 3)
        bear = (bear1 << 8) + bear2
        bear = bear/10.0
        #if bear > 180:
        #    bear = bear - 360
        return bear

def get_pitch():
        pitch = bus.read_byte_data(address, 4)
        pitch = (pitch * 360) / 255
        if pitch > 180:
            pitch = pitch - 360
        return pitch

class RefreshCoord(Thread):
    def __init__(self, mot):
        Thread.__init__(self)
        self.mot = mot

    def run(self):

        global astrobjectaltaz
        global altitude
        global azimut
        
        while thr_run:
            if rt_tracking == 1:
                rouen_gps = EarthLocation(lat=49.44*u.deg, lon=1.099*u.deg, height=21*u.m)
                utcoffset = 0*u.hour  # Paris
                time = Time.now() + utcoffset
                try:
                    target = SkyCoord.from_name(targetobject)
                except Exception as e:
                    target = get_body(targetobject,time)
                astrobjectaltaz = target.transform_to(AltAz(obstime=time,location=rouen_gps))
                altitude = float("{0.alt}".format(astrobjectaltaz).split(" ")[0])
                azimut = float("{0.az}".format(astrobjectaltaz).split(" ")[0])
                #print('synchro')
            sleep(1)

print("SNT Startup ...")
bus = smbus.SMBus(1)
address = 0x60

print("Checking arguments ...")
parser = argparse.ArgumentParser()
#parser.add_argument("horiz", type=float, help="azimut")
#parser.add_argument("vert", type=float, help="altitude")
parser.add_argument("type_p", help="type : c or b")
parser.add_argument("horiz", help="c:azimut b:body")
parser.add_argument("vert",  help="c:altitude b:dummy ")
args = parser.parse_args()
p_type = args.type_p
p_azimut = args.horiz
p_altitude = args.vert

print("Arg1 : ")
print(p_type)
print("Arg2 : ")
print(p_azimut)
print("Arg3 : ")
print(p_altitude)

if p_type == 'b':
    targetobject = p_azimut
    print("Inititializing target with " + targetobject)
    rouen_gps = EarthLocation(lat=49.44*u.deg, lon=1.099*u.deg, height=21*u.m)
    utcoffset = 0*u.hour  # Paris
    time = Time.now() + utcoffset
    try:
        target = SkyCoord.from_name(targetobject)
    except Exception as e:
        target = get_body(targetobject,time)
    targetaltaz = target.transform_to(AltAz(obstime=time,location=rouen_gps))
    print(targetobject + "'s Altitude = {0.alt}".format(targetaltaz))
    print(targetobject + "'s Azimut = {0.az}".format(targetaltaz))
    print(time)
    altitude = float("{0.alt}".format(targetaltaz).split(" ")[0])
    azimut = float("{0.az}".format(targetaltaz).split(" ")[0])
elif p_type == 'c':
    targetobject = 'Manual'
    azimut = float(p_azimut)
    altitude = float(p_altitude)


            
print(targetobject + " coordonates calculted")

thr_run = 1

print("Init Serial ...")
bno = BNO055.BNO055(serial_port='/dev/serial0', rst=18)
# Initialize the BNO055 and stop if something went wrong.
while True:
    try:
        if not bno.begin():
            raise RuntimeError('Failed to initialize BNO055! Is the sensor connected?')
        status, self_test, error = bno.get_system_status()
        break
    except Exception as e:
        print("Got error: {}".format(e))
        print("Sleep 1s")
        sleep(.1)
        
print('Reading BNO055 data, press Ctrl-C to quit...')

print('Getting calibration file')
while True:
    try:
        fichier = open(".calib_bno.ini",'r')
        str2 = fichier.readline()
        bytesArr = [int(x) for x in str2.split(' ')]
        print(bytesArr)
        calib_data = bno.set_calibration(bytesArr)
        fichier.close
        break
    except Exception as e:
        print("Got error: {}".format(e))
        print("Sleep 1s")
        sleep(.1)

print('calibration done')


#video
cap = cv2.VideoCapture(0) # get video source from pi system's first available camera (0)

# reducing resolution for quick processing
preview_width = 640
preview_height = 480
#capture_width = 3280
#capture_height = 2464
capture_width = 3200
capture_height = 2400

font = cv2.FONT_HERSHEY_SIMPLEX


cap.set(3,preview_width) # Width 
cap.set(4,preview_height) # Height

#azimut
Azimutstep = LED(20)
Azimutdir = LED(16)
Azimutdir.off()

#alt
Altistep = LED(19)
Altidir = LED(26)
Altidir.on()

#initialisation variable cible
limit = 200000
i = 0
erreur_near = .08
sign = 1

j = 0
erreur_az_near = .08
sign_az = 1


correction_alt = 0
correction_az = 0

manual = 0
loop = 0
rt_tracking = 0
lastdirection = 0
delay = .0005



print("Ready to start tracking " + targetobject)
print(azimut)
print(altitude)
#raw_input() 

if p_type == 'b':
    # Creation des threads
    thread_1 = RefreshCoord("canard")

    # Lancement des threads
    thread_1.start()

stdscr = curses.initscr()
curses.noecho()
curses.cbreak()
stdscr.keypad(1)
stdscr.addstr(0, 0, "Test ajout de texte a la con\r",curses.A_REVERSE)
stdscr.refresh()
stdscr.nodelay(1)

track_ok_alt = 0
track_ok_az = 0

try:
    while (i < limit) or (j < limit):
            ret,frame = cap.read() # get the frame from the capture source object (frame), ret isn't used
            frame = cv2.flip( frame, -1 )
            
            cv2.imshow('frame',frame) # create window showing captured frame
            
            
            sleep(delay)
            
            heading, roll, pitch = bno.read_euler()
            
            #pitch = get_pitch() - correction_alt
            pitch = pitch - correction_alt
            erreur_alt = altitude - pitch
            i = i + 1

            #direction = bearing3599()- correction_az
            direction = (heading+180) % 360 - correction_az
            
            erreur_az = azimut - direction
            
            print("%s %s %s\t%s %s %s\r" % (i ,pitch, erreur_alt,j ,direction, erreur_az))
            j = j + 1

            if manual == 0:
                if erreur_alt > 0:
                        Altidir.off()
                        sign = 1
                else:
                        Altidir.on()
                        sign = -1
                
                if (i < limit):
                        if (sign * erreur_alt) >= erreur_near:
                                Altistep.toggle()
                        else:
                                i = i + 1
                                track_ok_alt = 1

                if erreur_az > 0:
                        sign_az = 1
                        Azimutdir.off()
                else:
                        sign_az = -1
                        Azimutdir.on()
                
                if (j < limit):
                        if (sign_az * erreur_az) >= erreur_az_near:
                                Azimutstep.toggle()
                        else:
                                j = j + 1
                                if (track_ok_az == 0) and (rt_tracking == 0):
                                    rt_tracking = 1
                                track_ok_az = 1

            c = stdscr.getch()

            if c == curses.KEY_DOWN:
                    manual = 1
                    Altidir.on()
                    Altistep.toggle()
                    manual = 1
                    if loop == 1 and lastdirection == 1:
                            loop = 0
                    else:
                            loop = 1
                    lastdirection = 1
#                    print "down\r"

            elif c == curses.KEY_UP:
                    manual = 1
                    Altidir.off()
                    Altistep.toggle()
                    if loop == 1 and lastdirection == 2:
                            loop = 0
                    else:
                            loop = 1
                    lastdirection = 2
#                    print "up\r"

            elif c == curses.KEY_RIGHT:
                    manual = 1
                    Azimutdir.off()
                    Azimutstep.toggle()
                    if loop == 1 and lastdirection == 3:
                            loop = 0
                    else:
                            loop = 1
                    lastdirection = 3
#                    print "right\r"
        
            elif c == curses.KEY_LEFT:
                    manual = 1
                    Azimutdir.on()
                    Azimutstep.toggle()
                    if loop == 1 and lastdirection == 4:
                            loop = 0
                    else:
                            loop = 1
                    lastdirection = 4
                    
#                    print "left\r"

            elif c == ord('c'):
                    print "capture...\r"
                    cv2.putText(
                            frame, #numpy array on which text is written
                            "Python Examples", #text
                            (10,50), #position at which writing has to start
                            cv2.FONT_HERSHEY_SIMPLEX, #font family
                            1, #font size
                            (0, 0, 255), #B,G,R?
                            2) #font stroke
                    cv2.imshow('frame',frame) # create window showing captured frame
                    cap.set(3,capture_width) # Width 
                    cap.set(4,capture_height) # Height
                    ret,frame = cap.read() # get the frame from the capture source object (frame), ret isn't used
                    frame = cv2.flip( frame, -1 )
                    print time.strftime("%Y-%m-%d-%H-%M-%S")
                    cv2.imwrite("capture-"+time.strftime("%Y-%m-%d-%H-%M-%S")+".jpg",frame)
                    cap.set(3,preview_width) # Width 
                    cap.set(4,preview_height) # Height
                    print "...done !\r"
            elif c == ord(' '):
                    if manual == 1:
                        manual = 0
                        loop = 0
                        print "Switch to auto\r"
                    else:
                        manual = 1
                        print "Switch to manual\r"
            elif c == ord('t'):
                    if rt_tracking == 1:
                        rt_tracking = 0
                        print "Switch to rt_tracking\r"
                    else:
                        rt_tracking = 1
                        print "Switch to rt_tracking\r"
            elif c == ord('d'):
                    if delay == 0.0005:
                        delay = .04
                        print "delay to .004\r"
                    else:
                        delay = .0005
                        print "delay to .002\r"
                    

            elif (c == ord('q')) or (cv2.waitKey(1) & 0xFF == ord('q')):
                    print "quit\r"
                    break  # Exit the while()
            if loop == 1:
                    if lastdirection == 1:
                        Altidir.on()
                        Altistep.toggle()
                    if lastdirection == 2:
                        Altidir.off()
                        Altistep.toggle()
                    if lastdirection == 3:
                        Azimutdir.off()
                        Azimutstep.toggle()
                    if lastdirection == 4:
                        Azimutdir.on()
                        Azimutstep.toggle()
                
    #end
    thr_run = 0
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()
    cap.release() # release video object feed
    cv2.destroyAllWindows() # get rid of windows
    if p_type == 'b':
        thread_1.join()
    print("End tracking " + targetobject)
    print(azimut)
    print(altitude)

except KeyboardInterrupt:
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()
    cap.release() # release video object feed
    cv2.destroyAllWindows() # get rid of windows
    thr_run = 0
    if p_type == 'b':
        thread_1.join()
    print("End tracking " + targetobject)
    print(azimut)
    print(altitude)
    sys.exit()

