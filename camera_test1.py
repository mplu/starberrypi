from time import sleep
from picamera import PiCamera

camera = PiCamera()
camera.start_preview()
camera.resolution=(2592,1944)
# Camera warm-up time
camera.iso = 100
camera.framerate = 1
camera.exposure_mode = 'off'
# 1/100	: 10000
# 1/50	: 20000
# 1/40 	: 25000
# 1/20 	: 50000
# 1/10 	: 100000
camera.shutter_speed = 100000
camera.exposure_mode = 'off'
g = camera.awb_gains
camera.awb_mode = 'off'
camera.awb_gains = g
sleep(10)
camera.capture('foo_iso100_10000us.jpg')
