from picamera import PiCamera
from time import sleep

camera = PiCamera()
camera.preview_fullscreen=False
camera.preview_window=(0, 0, 1280, 1024)

camera.vflip=False
camera.hflip=False

camera.start_preview()
camera.resolution=(2592,1944)
sleep(60)

camera.stop_preview()


camera.close()

