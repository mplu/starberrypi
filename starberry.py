#!/usr/bin/python
# test3
import smbus
from gpiozero import LED
from time import sleep, strftime
from picamera import PiCamera
import logging
import sys
import time
import atexit
import math
import astropy.units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, get_body
from threading import Thread
from Adafruit_BNO055 import BNO055
import curses
import atexit
import numpy as np
import cv2
import numpy as np
import argparse

class RefreshCoord(Thread):
    def __init__(self, mot):
        Thread.__init__(self)
        self.mot = mot

    def run(self):

        global astrobjectaltaz
        global altitude
        global azimut
        
        while thr_run:
            if rt_tracking == 1:
                rouen_gps = EarthLocation(lat=49.44*u.deg, lon=1.099*u.deg, height=21*u.m)
                utcoffset = 0*u.hour  # Paris
                time = Time.now() + utcoffset
                try:
                    target = SkyCoord.from_name(targetobject)
                except Exception as e:
                    target = get_body(targetobject,time)
                astrobjectaltaz = target.transform_to(AltAz(obstime=time,location=rouen_gps))
                altitude = float("{0.alt}".format(astrobjectaltaz).split(" ")[0])
                azimut = float("{0.az}".format(astrobjectaltaz).split(" ")[0])
                #print('synchro')
            sleep(1)



print("SNT Startup ...")
manual = 0

print("Checking arguments ...")
parser = argparse.ArgumentParser()
parser.add_argument("type_v", help="type : p for picam driver, o for opencv, n for no display")
#parser.add_argument("type_track", help="type : f for fixed, t for track, s for single")
parser.add_argument("type_p", help="type : c or b")
parser.add_argument("horiz", help="c:azimut b:body")
parser.add_argument("vert",  help="c:altitude b:dummy ")
try:
    args = parser.parse_args()
    p_type_v = args.type_v
    p_type = args.type_p
    p_azimut = args.horiz
    p_altitude = args.vert
    manual = 1
except :
    print("default argument")
    p_type_v = "n"
    p_type = "c"
    p_azimut = "0"
    p_altitude = "0"

print("Arg1 : ")
print(p_type_v)
print("Arg2 : ")
print(p_type)
print("Arg3 : ")
print(p_azimut)
print("Arg4 : ")
print(p_altitude)

if p_type == 'b':
    targetobject = p_azimut
    print("Inititializing target with " + targetobject)
    rouen_gps = EarthLocation(lat=49.44*u.deg, lon=1.099*u.deg, height=21*u.m)
    utcoffset = 0*u.hour  # Paris
    time = Time.now() + utcoffset
    try:
        target = SkyCoord.from_name(targetobject)
    except Exception as e:
        target = get_body(targetobject,time)
    targetaltaz = target.transform_to(AltAz(obstime=time,location=rouen_gps))
    print(targetobject + "'s Azimut = {0.az}".format(targetaltaz))
    print(targetobject + "'s Altitude = {0.alt}".format(targetaltaz))
    print(time)
    altitude = float("{0.alt}".format(targetaltaz).split(" ")[0])
    azimut = float("{0.az}".format(targetaltaz).split(" ")[0])
elif p_type == 'c':
    targetobject = 'Manual'
    azimut = float(p_azimut)
    altitude = float(p_altitude)


            
print(targetobject + " coordonates calculted")

thr_run = 1

print("Init Serial ...")
bno = BNO055.BNO055(serial_port='/dev/serial0', rst=18)
# Initialize the BNO055 and stop if something went wrong.
while True:
    try:
        if not bno.begin():
            raise RuntimeError('Failed to initialize BNO055! Is the sensor connected?')
        status, self_test, error = bno.get_system_status()
        break
    except Exception as e:
        print("Got error: {}".format(e))
        print("Sleep 1s")
        sleep(.1)
        
print('Reading BNO055 data, press Ctrl-C to quit...')

print('Getting calibration file')
calibfile_attempt = 3
while calibfile_attempt > 0:
    try:
        fichier = open(".calib_bno.ini",'r')
        str2 = fichier.readline()
        bytesArr = [int(x) for x in str2.split(' ')]
        print(bytesArr)
        calib_data = bno.set_calibration(bytesArr)
        fichier.close
        break
    except Exception as e:
        print("Got error: {}".format(e))
        print("Sleep 1s")
        calibfile_attempt = calibfile_attempt - 1
        sleep(.1)
if calibfile_attempt <= 0:
    print('Calibration file not found, using last internal value')
else:
    print('Calibration done')

class RefreshIMU(Thread):
    def __init__(self, mot):
        Thread.__init__(self)
        self.mot = mot

    def run(self):

        global erreur_alt
        global erreur_az
        global pitch
        global direction
        global i_IMU
        global heading
        global pitch_raw
        
        while thr_run:
            i_IMU = i_IMU + 1
            heading, roll, pitch_raw = bno.read_euler()
            pitch = pitch_raw - offset_alt
            erreur_alt = altitude - pitch
            direction = (heading+180-offset_az ) % 360
            erreur_az = azimut - direction
            if erreur_az > 180:
                erreur_az = erreur_az - 360
            sleep(.05)


#video
#la capture retourne une erreur si le Rpi n'a pas assez de memoire GPU
#128mo sont ok pour 1600x1200
#256mo sont ok pour 4000x3000
                    
preview_width = 800
preview_height = 600
capture_width = 4000
capture_height = 3000
try:
    if p_type_v == 'p':
        camera = PiCamera()
        camera.preview_fullscreen=False
        camera.preview_window=(1440-preview_width, 70, preview_width, preview_height)
        camera.start_preview()
        camera.resolution=(capture_width,capture_height)
    elif p_type_v == 'o':
        cap = cv2.VideoCapture(0) # get video source from pi system's first available camera (0)
        # reducing resolution for quick processing
        font = cv2.FONT_HERSHEY_SIMPLEX
        cap.set(cv2.CAP_PROP_FRAME_WIDTH,preview_width) # Width 
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT,preview_height) # Height
    elif p_type_v == 'n':
       print("Image externe")


    class RefreshOCV(Thread):
        def __init__(self, mot):
            Thread.__init__(self)
            self.mot = mot

        def run(self):

            global frame
            global cropped

            while thr_run:
                ret,frame = cap.read() # get the frame from the capture source object (frame), ret isn't used
                #frame = cv2.flip( frame, -1 )
                if cropped == True :
                    frame = frame[preview_width/2-40:preview_width/2+40,preview_height/2-30:preview_height/2+30]
                cv2.imshow('frame',frame) # create window showing captured frame
                sleep(.2)

except Exception as e:
    print("Got error: {}".format(e))
    print("Impossible d'ouvrir la camera. Un soft externe est il lance ?")
    quit()




#azimut
Azimutstep = LED(20) #BCM20 - pin 38
Azimutdir = LED(16) #BCM16 - pin 36
Azimutdir.off()
Azimutms2 = LED(21) #BCM21
Azimutms2.off()

#alt
Altistep = LED(19) #BCM19 - pin 35
Altidir = LED(26) #BCM26 - pin 37
Altidir.on()
Altims2 = LED(13) #BCM13
Altims2.off()




#initialisation variable cible
limit = 200000
i = 0
i_IMU = 0
erreur_near = .5
erreur = 0.03
sign = 1

erreur_az_near = erreur_near
sign_az = 1

try:
	fichier = open(".param_offset.ini",'r')
	input_string = fichier.read()
	offset_az = float(input_string.split(" ")[0])
	offset_alt = float(input_string.split(" ")[1])
	print("found offset file")
	print(offset_az)
	print(offset_alt)
	fichier.close
except Exception as e:
    print("pas de fichier")
    offset_alt = 0
    offset_az = 0

cropped = False
manual = 0
loop = 0
i = 0
rt_tracking = 0
lastdirection = 0
delay_hi = .002
delay_lo = .03
delay = delay_hi
mode_precis = 0
freqIMU = 0
freqmain = 0
gohaut = 0
gobas = 0
gogauche = 0
godroite = 0

print("Ready to start tracking " + targetobject)
print(azimut)
print(altitude)
#raw_input() 

class RefreshFreqCalc(Thread):
    def __init__(self, mot):
        Thread.__init__(self)
        self.mot = mot

    def run(self):

        global freqmain
        global freqIMU

        i_prec = 0
        i_IMU_prec = 0
        while thr_run:
            if i > i_prec:
                freqmain = i - i_prec
            if i > i_prec:
                freqIMU = i_IMU - i_IMU_prec
            i_prec = i
            i_IMU_prec = i_IMU
            sleep(1)

if p_type == 'b':
    # Creation des threads
    thread_1 = RefreshCoord("Coordonnees")
    # Lancement des threads
    thread_1.start()

# Creation des threads
thread_2 = RefreshIMU("IMU")
# Lancement des threads
thread_2.start()

if p_type_v == 'o':
    # Creation des threads
    thread_3 = RefreshOCV("video opencv")
    # Lancement des threads
    thread_3.start()
    sleep(.5)

# Creation des threads
thread_4 = RefreshFreqCalc("Freq")
# Lancement des threads
thread_4.start()

track_ok_alt = 0
track_ok_az = 0

stdscr = curses.initscr()
curses.noecho()
curses.cbreak()
stdscr.keypad(1)
stdscr.addstr(0, 0, "Demarrage du guidage...",curses.A_BOLD)
stdscr.refresh()
stdscr.nodelay(1)


try:
    while (i < limit):

            
            
            sleep(delay)
            
            i = i + 1
            

            
            #print("%s %s\t%s %s\r" % (direction, erreur_az,pitch, erreur_alt))

            stdscr.addstr(0, 0, "Mode      : ",curses.A_NORMAL)
            if p_type == 'b':
                stdscr.addstr(0, 12, "Corp celeste",curses.A_NORMAL)
                stdscr.addstr(0, 30, 'Corp suivi : {}'.format(targetobject),curses.A_NORMAL)
                
            elif p_type == 'c':
                stdscr.addstr(0, 12, "Coordonnes   ",curses.A_NORMAL)

            
            stdscr.addstr(1, 0, "Affichage : ",curses.A_NORMAL)
            if p_type_v == 'o':
                stdscr.addstr(1, 12, "opencv     ",curses.A_NORMAL)
            elif p_type_v == 'p':
                stdscr.addstr(1, 12, "picamera   ",curses.A_NORMAL)
            elif p_type_v == 'n':
                stdscr.addstr(1, 12, "non       ",curses.A_NORMAL)
            
            if rt_tracking == 1:
                stdscr.addstr(2, 30, "RT Tracking : Oui",curses.A_NORMAL)
            else:
                stdscr.addstr(2, 30, "RT Tracking : Non   ",curses.A_NORMAL)
            if manual == 1:
                stdscr.addstr(3, 30, "Mode Manuel : Oui",curses.A_NORMAL)
            else:
                stdscr.addstr(3, 30, "Mode Manuel : Non   ",curses.A_NORMAL)
            
            if gohaut == 1:
                stdscr.addstr(4, 12,"^",curses.A_BOLD)
            else:
                stdscr.addstr(4, 12,"-",curses.A_BOLD)
            if gobas == 1:
                stdscr.addstr(6, 12,"v",curses.A_BOLD)
            else:
                stdscr.addstr(6, 12,"-",curses.A_BOLD)
            if gogauche == 1:
                stdscr.addstr(5, 11,"<o",curses.A_BOLD)
            else:
                stdscr.addstr(5, 11,"|o",curses.A_BOLD)
            if godroite == 1:
                stdscr.addstr(5, 13,">",curses.A_BOLD)
            else:
                stdscr.addstr(5, 13,"|",curses.A_BOLD)
            
            stdscr.addstr(4, 30,     'Delai       : {}        '.format(delay),curses.A_NORMAL)
            stdscr.addstr(5, 30,     'Seuil Err   : {}        '.format(erreur),curses.A_NORMAL)
            if mode_precis == 1:
                stdscr.addstr(6, 30, "Mode Precis : Oui     ",curses.A_NORMAL)
            else:
                stdscr.addstr(6, 30, "Mode Precis : Non   ",curses.A_NORMAL)
            stdscr.addstr(8, 0,'Coordonnees suivi  : {0:4.9f}\t{1:4.9f}        '.format(azimut,altitude),curses.A_NORMAL)
            stdscr.addstr(9, 0,'Coordonnees visees : {0:4.9f}\t{1:4.9f}        '.format(direction,pitch),curses.A_BOLD)
            stdscr.addstr(10, 0,'            erreur : {0:4.9f}\t{1:4.9f}        '.format(erreur_az,erreur_alt),curses.A_NORMAL)
            stdscr.addstr(11, 0,'            offset : {0:4.9f}\t{1:4.9f}        '.format(offset_az,offset_alt),curses.A_NORMAL)
            stdscr.addstr(16, 0, "i_main:{} - {}Hz           ".format(i,freqmain),curses.A_NORMAL)
            stdscr.addstr(17, 0, "i_IMU :{} - {}Hz            ".format(i_IMU,freqIMU),curses.A_NORMAL)
            
            
            
            
            
            if manual == 0:
                if erreur_alt > 0:
                    Altidir.off()
                    sign = 1
                else:
                    Altidir.on()
                    sign = -1
                
                if (sign * erreur_alt) >= erreur:
                    Altistep.toggle()
                    if sign == 1:
                        gohaut = 1
                        gobas = 0
                    else:
                        gohaut = 0
                        gobas = 1
                else:
                    track_ok_alt = 1
                    gohaut = 0
                    gobas = 0

                if erreur_az > 0:
                    sign_az = 1
                    Azimutdir.off()
                else:
                    sign_az = -1
                    Azimutdir.on()
                
                if (sign_az * erreur_az) >= erreur:
                    Azimutstep.toggle()
                    if sign_az == 1:
                        gogauche = 0
                        godroite = 1
                    else:
                        gogauche = 1
                        godroite = 0
                else:
                    if (track_ok_az == 0) and (rt_tracking == 0):
                        rt_tracking = 1
                    track_ok_az = 1
                    gogauche = 0
                    godroite = 0
                
                if (((sign * erreur_alt) < erreur_near) and ((sign * erreur_az) < erreur_near)):
                    mode_precis = 1
                    delay = delay_lo
                else:
                    mode_precis = 0
                    delay = delay_hi

            c = stdscr.getch()

            if c == curses.KEY_DOWN:
                manual = 1
                Altidir.on()
                Altistep.toggle()
                manual = 1
                if loop == 1 and lastdirection == 1:
                    loop = 0
                    gobas = 0
                else:
                    loop = 1
                lastdirection = 1
                stdscr.addstr(15, 0, "down            ",curses.A_NORMAL)

            elif c == curses.KEY_UP:
                manual = 1
                Altidir.off()
                Altistep.toggle()
                if loop == 1 and lastdirection == 2:
                    loop = 0
                    gohaut = 0
                else:
                    loop = 1
                lastdirection = 2
                stdscr.addstr(15, 0, "up            ",curses.A_NORMAL)

            elif c == curses.KEY_RIGHT:
                manual = 1
                Azimutdir.off()
                Azimutstep.toggle()
                if loop == 1 and lastdirection == 3:
                    loop = 0
                    godroite = 0
                else:
                    loop = 1
                lastdirection = 3
                stdscr.addstr(15, 0, "right            ",curses.A_NORMAL)
        
            elif c == curses.KEY_LEFT:
                manual = 1
                Azimutdir.on()
                Azimutstep.toggle()
                if loop == 1 and lastdirection == 4:
                    loop = 0
                    goleft = 0
                else:
                    loop = 1
                lastdirection = 4
                stdscr.addstr(15, 0, "left            ",curses.A_NORMAL)

            elif c== ord('z'):
                if cropped == False:
                    cropped = True
                else:
                    cropped = False

            elif c == ord('c'):
                stdscr.addstr(15, 0, "capture            ",curses.A_NORMAL)
                if (p_type_v == 'p'):
                    stdscr.addstr(15, 0, "picamera            ",curses.A_NORMAL)
                    camera.capture("./capture-"+strftime("%Y-%m-%d-%H-%M-%S")+".jpg")
                elif p_type_v == 'o':
                    #la capture retourne "vidioc_streamon operation not permitted" si le Rpi n'a pas assez de memoire GPU
                    #128mo sont ok pour 1600x1200
                    #256mo sont ok pour 4000x3000
                    stdscr.addstr(15, 0, "opencv            ",curses.A_NORMAL)
                    cv2.putText(
                        frame, #numpy array on which text is written
                        "Capture", #text
                        (10,50), #position at which writing has to start
                        cv2.FONT_HERSHEY_SIMPLEX, #font family
                        1, #font size
                        (0, 0, 255), #B,G,R?
                        2) #font stroke
                    cv2.imshow('frame',frame) # create window showing captured frame
                    #cap.set(cv2.CAP_PROP_FRAME_WIDTH,3200) # Width 
                    #cap.set(cv2.CAP_PROP_FRAME_HEIGHT,2400) # Height
                    cap.set(cv2.CAP_PROP_FRAME_WIDTH,capture_width) # Width 
                    cap.set(cv2.CAP_PROP_FRAME_HEIGHT,capture_height) # Height
                    ret,frame = cap.read() # get the frame from the capture source object (frame), ret isn't used
                    cv2.imwrite("./capture-"+strftime("%Y-%m-%d-%H-%M-%S")+".jpg",frame)
                    cap.set(3,preview_width) # Width 
                    cap.set(4,preview_height) # Height
                stdscr.addstr(15, 0, "capture done!            ",curses.A_NORMAL)
            elif c == ord(' '):
                if manual == 1:
                    manual = 0
                    loop = 0
                    stdscr.addstr(15, 0, "Switch to auto            ",curses.A_NORMAL)
                else:
                    manual = 1
                    stdscr.addstr(15, 0, "Switch to manual            ",curses.A_NORMAL)
            elif c == ord('t'):
                if rt_tracking == 1:
                    rt_tracking = 0
                    stdscr.addstr(15, 0, "Switch to rt_tracking off            ",curses.A_NORMAL)
                else:
                    rt_tracking = 1
                    stdscr.addstr(15, 0, "Switch to rt_tracking on            ",curses.A_NORMAL)
            elif c == ord('d'):
                if delay == delay_lo:
                    delay = delay_hi
                    stdscr.addstr(15, 0, 'Delai : {}            '.format(delay),curses.A_NORMAL)
                else:
                    delay = delay_lo
                    stdscr.addstr(15, 0, 'Delai : {}           '.format(delay),curses.A_NORMAL)
            elif c == ord('s'):
                if mode_precis == 1:
                    mode_precis = 0
                    stdscr.addstr(15, 0, 'Mode : rapide            ',curses.A_NORMAL)
                    Azimutms2.off()
                    Altims2.off()
                else:
                    mode_precis = 1
                    stdscr.addstr(15, 0, 'Mode : precis            ',curses.A_NORMAL)
                    Azimutms2.on()
                    Altims2.on()
            elif c == ord('o'):
                #offset_az = heading - 180 - azimut
                offset_az = (heading+180-azimut) % 360
                offset_alt = pitch_raw - altitude
                fichier = open(".param_offset.ini",'w')
                fichier.write(str(offset_az) + " " + str(offset_alt))
                fichier.close
            elif c == ord('i'):
                offset_az = 0.0
                offset_alt = 0.0
                fichier = open(".param_offset.ini",'w')
                fichier.write(str(offset_az) + " " + str(offset_alt))
                fichier.close
            elif c == ord('q'):
                stdscr.addstr(15, 0, "quit            ",curses.A_NORMAL)
                break  # Exit the while()
            elif (p_type_v == 'o'):
                if (cv2.waitKey(1) & 0xFF == ord('q')):
                    break  # Exit the while()
            if loop == 1:
                if lastdirection == 1:
                    Altidir.on()
                    Altistep.toggle()
                    gobas = 1
                    gohaut = 0
                    godroite = 0
                    gogauche = 0
                if lastdirection == 2:
                    Altidir.off()
                    Altistep.toggle()
                    gobas = 0
                    gohaut = 1
                    godroite = 0
                    gogauche = 0
                if lastdirection == 3:
                    Azimutdir.off()
                    Azimutstep.toggle()
                    gobas = 0
                    gohaut = 0
                    godroite = 1
                    gogauche = 0
                if lastdirection == 4:
                    Azimutdir.on()
                    Azimutstep.toggle()
                    gobas = 0
                    gohaut = 0
                    godroite = 0
                    gogauche = 1
            #else:
                #gobas = 0
                #gohaut = 0
                #godroite = 0
                #gogauche = 0
                
    #end
    thr_run = 0
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()
    if p_type_v == 'p':
        camera.stop_preview()
        camera.close()
    elif p_type_v == 'o':
        cap.release() # release video object feed
        cv2.destroyAllWindows() # get rid of windows
        thread_3.join()
    if p_type == 'b':
        thread_1.join()
    thread_2.join()
    print("End tracking " + targetobject)
    print(azimut)
    print(altitude)

except KeyboardInterrupt:
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()
    if p_type_v == 'p':
        camera.stop_preview()
        camera.close()
    elif p_type_v == 'o':
        cap.release() # release video object feed
        cv2.destroyAllWindows() # get rid of windows
        thread_3.join()
    thr_run = 0
    if p_type == 'b':
        thread_1.join()
    thread_2.join()
    print("End tracking " + targetobject)
    print(azimut)
    print(altitude)
    sys.exit()

