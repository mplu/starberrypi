#!/usr/bin/python
# test3
import smbus
from gpiozero import LED
from time import sleep, strftime
import sys
import time
import atexit
import math
from threading import Thread
import curses
import atexit
import argparse





print("Motor Controle Startup ...")

print("Checking arguments ...")
#parser = argparse.ArgumentParser()
#parser.add_argument("type_v", help="type : p for picam driver, o for opencv, n for no display")
#parser.add_argument("type_p", help="type : c or b")
#parser.add_argument("horiz", help="c:azimut b:body")
#parser.add_argument("vert",  help="c:altitude b:dummy ")
#args = parser.parse_args()
#p_type_v = args.type_v
#p_type = args.type_p
#p_azimut = args.horiz
#p_altitude = args.vert

#print("Arg1 : ")
#print(p_type_v)
#print("Arg2 : ")
#print(p_type)
#print("Arg3 : ")
#print(p_azimut)
#print("Arg4 : ")
#print(p_altitude)



thr_run = 1
        







#azimut
Azimutstep = LED(20) #BCM20 - pin 38
Azimutdir = LED(16) #BCM16 - pin 36
Azimutdir.off()
Azimutms2 = LED(21) #BCM21
Azimutms2.off()

#alt
Altistep = LED(19) #BCM19 - pin 35
Altidir = LED(26) #BCM26 - pin 37
Altidir.on()
Altims2 = LED(13) #BCM13
Altims2.off()




#initialisation variable cible
limit = 200000
i = 0
#i_IMU = 0
#erreur_near = .5
#erreur = 0.08
#sign = 1

#erreur_az_near = erreur_near
#sign_az = 1


cropped = False
manual = 1
loop = 0
i = 0
rt_tracking = 0
lastdirection = 0
delay_hi = .001
delay_lo = .03
delay = delay_hi
mode_precis = 0
freqIMU = 0
freqmain = 0
gohaut = 0
gobas = 0
gogauche = 0
godroite = 0



class RefreshFreqCalc(Thread):
    def __init__(self, mot):
        Thread.__init__(self)
        self.mot = mot

    def run(self):

        global freqmain



        i_prec = 0
        while thr_run:
            if i > i_prec:
                freqmain = i - i_prec
            i_prec = i

            sleep(1)





# Creation des threads
thread_4 = RefreshFreqCalc("Freq")
# Lancement des threads
thread_4.start()

track_ok_alt = 0
track_ok_az = 0

stdscr = curses.initscr()
curses.noecho()
curses.cbreak()
stdscr.keypad(1)
stdscr.addstr(0, 0, "Demarrage du guidage...",curses.A_BOLD)
stdscr.refresh()
stdscr.nodelay(1)


try:
    while (i < limit):

            
            
            sleep(delay)
            
            i = i + 1
            

            
            #print("%s %s\t%s %s\r" % (direction, erreur_az,pitch, erreur_alt))

            stdscr.addstr(0, 0, "Mode      : ",curses.A_NORMAL)
            stdscr.addstr(0, 12, "Test Moteur   ",curses.A_NORMAL)

            
            stdscr.addstr(1, 0, "Affichage : ",curses.A_NORMAL)
            stdscr.addstr(1, 12, "non       ",curses.A_NORMAL)
            
            
            if gohaut == 1:
                stdscr.addstr(4, 12,"^",curses.A_BOLD)
            else:
                stdscr.addstr(4, 12,"-",curses.A_BOLD)
            if gobas == 1:
                stdscr.addstr(6, 12,"v",curses.A_BOLD)
            else:
                stdscr.addstr(6, 12,"-",curses.A_BOLD)
            if gogauche == 1:
                stdscr.addstr(5, 11,"<o",curses.A_BOLD)
            else:
                stdscr.addstr(5, 11,"|o",curses.A_BOLD)
            if godroite == 1:
                stdscr.addstr(5, 13,">",curses.A_BOLD)
            else:
                stdscr.addstr(5, 13,"|",curses.A_BOLD)
            
            stdscr.addstr(4, 30,     'Delai       : {}        '.format(delay),curses.A_NORMAL)
            if mode_precis == 1:
                stdscr.addstr(6, 30, "Mode Precis : Oui     ",curses.A_NORMAL)
            else:
                stdscr.addstr(6, 30, "Mode Precis : Non   ",curses.A_NORMAL)
            stdscr.addstr(16, 0, "i_main:{} - {}Hz           ".format(i,freqIMU),curses.A_NORMAL)
            
            
            
            
            
            if manual == 0:
                if erreur_alt > 0:
                    Altidir.off()
                    sign = 1
                else:
                    Altidir.on()
                    sign = -1
                
                if (sign * erreur_alt) >= erreur:
                    Altistep.toggle()
                    if sign == 1:
                        gohaut = 1
                        gobas = 0
                    else:
                        gohaut = 0
                        gobas = 1
                else:
                    track_ok_alt = 1
                    gohaut = 0
                    gobas = 0

                if erreur_az > 0:
                    sign_az = 1
                    Azimutdir.off()
                else:
                    sign_az = -1
                    Azimutdir.on()
                
                if (sign_az * erreur_az) >= erreur:
                    Azimutstep.toggle()
                    if sign_az == 1:
                        gogauche = 0
                        godroite = 1
                    else:
                        gogauche = 1
                        godroite = 0
                else:
                    if (track_ok_az == 0) and (rt_tracking == 0):
                        rt_tracking = 1
                    track_ok_az = 1
                    gogauche = 0
                    godroite = 0
                
                if (((sign * erreur_alt) < erreur_near) and ((sign * erreur_az) < erreur_near)):
                    mode_precis = 1
                else:
                    mode_precis = 0

            c = stdscr.getch()

            if c == curses.KEY_DOWN:
                manual = 1
                Altidir.on()
                Altistep.toggle()
                manual = 1
                if loop == 1 and lastdirection == 1:
                    loop = 0
                    gobas = 0
                else:
                    loop = 1
                lastdirection = 1
                stdscr.addstr(15, 0, "down            ",curses.A_NORMAL)

            elif c == curses.KEY_UP:
                manual = 1
                Altidir.off()
                Altistep.toggle()
                if loop == 1 and lastdirection == 2:
                    loop = 0
                    gohaut = 0
                else:
                    loop = 1
                lastdirection = 2
                stdscr.addstr(15, 0, "up            ",curses.A_NORMAL)

            elif c == curses.KEY_RIGHT:
                manual = 1
                Azimutdir.off()
                Azimutstep.toggle()
                if loop == 1 and lastdirection == 3:
                    loop = 0
                    godroite = 0
                else:
                    loop = 1
                lastdirection = 3
                stdscr.addstr(15, 0, "right            ",curses.A_NORMAL)
        
            elif c == curses.KEY_LEFT:
                manual = 1
                Azimutdir.on()
                Azimutstep.toggle()
                if loop == 1 and lastdirection == 4:
                    loop = 0
                    goleft = 0
                else:
                    loop = 1
                lastdirection = 4
                stdscr.addstr(15, 0, "left            ",curses.A_NORMAL)

            
            elif c == ord('d'):
                if delay == delay_lo:
                    delay = delay_hi
                    stdscr.addstr(15, 0, 'Delai : {}            '.format(delay),curses.A_NORMAL)
                else:
                    delay = delay_lo
                    stdscr.addstr(15, 0, 'Delai : {}           '.format(delay),curses.A_NORMAL)
            elif c == ord('s'):
                if mode_precis == 1:
                    mode_precis = 0
                    stdscr.addstr(15, 0, 'Mode : rapide            ',curses.A_NORMAL)
                    Azimutms2.off()
                    Altims2.off()
                else:
                    mode_precis = 1
                    stdscr.addstr(15, 0, 'Mode : precis            ',curses.A_NORMAL)
                    Azimutms2.on()
                    Altims2.on()
            
            elif c == ord('q'):
                stdscr.addstr(15, 0, "quit            ",curses.A_NORMAL)
                break  # Exit the while()
            
            if loop == 1:
                if lastdirection == 1:
                    Altidir.on()
                    Altistep.toggle()
                    gobas = 1
                    gohaut = 0
                    godroite = 0
                    gogauche = 0
                if lastdirection == 2:
                    Altidir.off()
                    Altistep.toggle()
                    gobas = 0
                    gohaut = 1
                    godroite = 0
                    gogauche = 0
                if lastdirection == 3:
                    Azimutdir.off()
                    Azimutstep.toggle()
                    gobas = 0
                    gohaut = 0
                    godroite = 1
                    gogauche = 0
                if lastdirection == 4:
                    Azimutdir.on()
                    Azimutstep.toggle()
                    gobas = 0
                    gohaut = 0
                    godroite = 0
                    gogauche = 1
            #else:
                #gobas = 0
                #gohaut = 0
                #godroite = 0
                #gogauche = 0
                
    #end
    thr_run = 0
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()
    print("End tracking " )
    
except KeyboardInterrupt:
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()
    thr_run = 0
    print("End tracking " )
    sys.exit()

